Usage:

move invoiceTemplate.tex.default to ~/Templates/texInvoice/invoiceTemplate.tex

rename invoice.ini.default invoice.ini and fill in proper values

maybe tweak the paths a little?
//this needs work



```
usage: main.py [-h] [-e [EXPORTPATH]] [-t [TITLE]] inputPath

Produce invoice.pdf from TeX Template.

positional arguments:
  inputPath        path to the hours file

optional arguments:
  -h, --help       show this help message and exit
  -e [EXPORTPATH]  folder to export pdf to
  -t [TITLE]       document title
```
