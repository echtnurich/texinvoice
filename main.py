import open,argparse,datetime,subprocess,configparser
from os.path import expanduser

# PATHS
homePath = expanduser("~")
# set the location of main.py and invoice.ini here
basePath = homePath + "/Documents/git/lora"

hoursFilePath = basePath + "/texInvoice/hours.txt"
templatePath = homePath+"/Templates/texInvoice/invoiceTemplate.tex"
outputPath = "/tmp/texInvoice.tex"

# VARS
outEntryTable = ""
sumHours = 0
now = datetime.datetime.now()

# CONFIGPARSER
config = configparser.ConfigParser()
config.read(basePath + "/texInvoice/invoice.ini")
configMap = {"--SENDERNAME--":config['sender']['name'],
             "--SENDERSTREET--":config['sender']['street'],
             "--SENDERCITY--":config['sender']['city'],
             "--SENDEREMAIL--":config['sender']['email'],
             "--RECIPNAME--":config['recipient']['name'],
             "--RECIPCOMP--":config['recipient']['company'],
             "--RECIPSTREET--":config['recipient']['street'],
             "--RECIPCITY--":config['recipient']['city'],
             "--LETOPEN--":config['letter']['opening'],
             "--LETBODY1--":config['letter']['body1'],
             "--TABLEHEAD--":config['table']['head'],
             "--TABLEFOOT--":config['table']['foot'],
             "--LETTAXNOT--":config['letter']['taxnotice'],
             "--LETPROMPT--":config['letter']['prompt'],
             "--LETBODY2--":config['letter']['body2'],
             "--LETCLOSE--":config['letter']['closing'],
             "--BANKNAME--":config['bank']['name'],
             "--BANKIBAN--":config['bank']['iban'],
             "--BANKBIC--":config['bank']['bic'],
             "--TITLE--":config['settings']['title'],
             "--INVDATE--":config['settings']['invdate']
            }

#ARGPARSE
parser = argparse.ArgumentParser(description='Produce invoice.pdf from TeX Template.')
parser.add_argument('hours file', metavar='inputPath', type=str,
                    help='path to the hours file')
parser.add_argument('-e', nargs='?', dest='exportPath',
                    help='folder to export pdf to')
parser.add_argument('-d', nargs='?', dest='date', default=config['settings']['invdate'],
                    help='invoice date coverage')
args = parser.parse_args()

# define template modifications - line by line
def replacePlaceholders(template,outFile,configuration,entries,total,date):
    configuration.update([("--ENTRIES--",entries),("--TOTAL--",str(total)),("--INVDATE--",date)])
    for line in template:
        for element in configuration:
            line = line.replace(element,str(configuration[element]))
        outFile.write(line)
    

# convert hours file to LaTeX table
# line by line, splitting lines at ;, inserting & inbetween, \\ at the end of the line
for entry in open.hoursFile(hoursFilePath).read().splitlines():
    entrySegment = entry.split(";")
    try:
        entrySegment.append(int(entrySegment[3])*int(entrySegment[4]))
        sumHours      += int(entrySegment[3])*int(entrySegment[4])
    except:
        pass

    for segment in entry.split(";"):
        outEntryTable += segment + " &"
    outEntryTable = outEntryTable[0:-1]
    outEntryTable +=  " & " + str(entrySegment[-1]) + "\\\\\n"

# run
replacePlaceholders(open.readTemplate(templatePath),
                    open.writeFile(outputPath),
                    configMap,
                    outEntryTable,
                    sumHours,
                    args.date)

# should i export?
if args.exportPath != None:
    subprocess.call(["pdflatex","-output-directory=/tmp","/tmp/texInvoice.tex"])
    subprocess.call(["mv","/tmp/texInvoice.pdf",args.exportPath])
